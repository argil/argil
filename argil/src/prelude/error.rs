use std::fmt;

pub struct ArgilErr;

impl fmt::Display for ArgilErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "An error has occured with the Argil runtime")
    }
}

impl fmt::Debug for ArgilErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{{ file: {}, line: {} }}", file!(), line!())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn produce_err() -> Result<(), super::ArgilErr> {
        Err(super::ArgilErr)
    }
}
