/// A struct that represents a line in a document.
/// 
/// @param ```num``` The int representation of where the line is in the document. Starts at 0.
pub (crate) struct Line {
    pub num: i32,
    pub len: usize,
    pub body: Vec<String>,
}

impl Line {
    /// ## Initialized the Line object
    pub fn new(num: i32, len: usize, body: Vec<String>) -> Self {
        Self {
            num,
            len,
            body,
        }
    }
}