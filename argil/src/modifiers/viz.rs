pub enum FuncMods {
    Public,
    Local,
    Export,
}

impl FuncMods {
    pub fn parse(string: String) -> Option<Self> {
       match string.as_str() {
            "pub" | "public" => Some(Self::Public),
            "loc" | "local" => Some(Self::Local),
            "exp" => Some(Self::Export),
            _ => None
        }
    }
}
