use crate::scope::var::Var;
use crate::scope::function::ArgilFunc;
use std::collections::BTreeMap;
use std::boxed::Box;



pub struct State {
    scope: usize,
    vars: Option<BTreeMap<String, Var>>,
    func: Option<BTreeMap<String, ArgilFunc>>,
    n_state: Option<Box<Self>>
}

impl State {
    pub fn new(scope: usize, vars: Option<BTreeMap<String, Var>>, func: Option<BTreeMap<String, ArgilFunc>>, n_state: Option<Box<Self>>) -> Self {
        Self {
            scope,
            vars,
            func,
            n_state
        }
    }
}