use std::env;
use std::path::Path;
use argil::lang::Argil;


fn main() {
    let argv: Vec<String> = env::args().collect();
    // Gets the target line arguments and maps them to variables for easy reference.
    if argv.len() == 2 {
        let target = &argv[1];
        let cmd_path = Path::new(target);
        match cmd_path.is_file() {
            true => {
                drop(cmd_path);
                let _argil = Argil::read_to_ast(&format!("./{}", target));
            }
            false => {
                drop(cmd_path);
                if target == "--help" || target == "-h" {
                    print_help();
                } else if target == "--v" || target == "-v" || target == "--version" {
                    println!("{} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
                // Add Build code Below here!
                } else {
                    print_help();
                }
            } // obvious
        };
    } else {
        print_help();
    }
}
/// Prints the CLI help message
fn print_help() {
    println!("To run a file directly:");
    println!("  $ {} filename.clay", env!("CARGO_PKG_NAME"));
    // println!("To compile an entire dir recursivly");
    // println!("  $ {} --dir target-dir", env!("CARGO_PKG_NAME"));
}

// Code to be used later!

/* } else if target == "build" || target == "b" {
                    let paths = fs::read_dir(env::current_dir().unwrap()).unwrap();
                    for path in paths {
                        let path_str = path.unwrap().path().into_os_string().into_string();
                        match &path_str {
                            Ok(s) => {
                                let doc = Doc::new(s.as_str());
                                if doc.is_main() {
                                    match doc.validate() {
                                        Ok(()) => println!("Build was a succsess!"),
                                        Err(e) => eprintln!("{}", e),
                                    }
                                }
                            }
                            Err(_e) => println!(
                                "Unable to parse {}. Err: {}",
                                &path_str.unwrap().to_string(),
                                "Unable to convert file to actionalble string"
                            ),
                        };
                    }*/